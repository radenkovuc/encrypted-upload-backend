const express = require("express");
const router = express.Router();

const multer = require("multer");
const upload = multer({});

const fileController = require("../controllers/file");


router.post("/file/upload", upload.single("file"), fileController.upload);

module.exports = router;
