"use strict";
module.exports = (sequelize, DataTypes) => {
	const File = sequelize.define(
		"File",
		{
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.INTEGER
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false
			},
			size: {
				type: DataTypes.INTEGER
			},
			buffer:{
				type: DataTypes.BLOB
			},
			createdAt: {
				allowNull: false,
				type: DataTypes.DATE
			},
			updatedAt: {
				allowNull: false,
				type: DataTypes.DATE
			}
		},
		{
			tableName: 'file'
		}
	);
	return File;
};
