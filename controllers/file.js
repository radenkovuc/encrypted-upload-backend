const {encrypt,decrypt}  = require("../utility/encription")

const {File} = require("../models");

const key = "12345678901234567890123456789012"


async function upload(req, res) {
	console.log("upload");
	try {
		const fileName = decrypt(req.file.originalname,key);
		const fileSize = Math.round(req.file.size / 1024);
		console.log("File name: ", fileName);
		console.log("File size: ", fileSize);
		console.log("File buffer: ", req.file.buffer.toString());

		const buffer = decrypt(req.file.buffer.toString(),key);

		try {
			const file = await File.create({
				name: fileName,
				size: fileSize,
				buffer:buffer
			});
		} catch (err) {
			/// ......

			throw new Error(err);
		}
		res.send("File uploaded");
	} catch (err) {
		console.log(err);
		res.status(400).send(err.message);
	}
}


module.exports = {  upload };
