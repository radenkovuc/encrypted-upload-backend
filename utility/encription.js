const crypto = require('crypto-js');


function encrypt(data,key) {
  return crypto.AES.encrypt(data,key);

}

function decrypt(data,key) {
  return crypto.AES.decrypt(data,key).toString(crypto.enc.Utf8);
}

module.exports = { encrypt, decrypt };
