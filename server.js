const express = require("express");
const bodyParser = require("body-parser");
const routes = require("./routes/index");
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(routes);

let port = 5000 || process.env.PORT;

app.listen(port, () => {
	console.log(`Server started at port: ${port}`);
});
